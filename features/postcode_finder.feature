Feature: Post Code finder

  A user of the public should be able to search for an address on a public web site and see what days postage is available,
  and what the post code is.

  Scenario: Partial address should have correct suggestions
    Given I visit the post code finder page
    When I type part of an address
    Then the correct address suggestion should be requested
