# frozen_string_literal: true

require 'watir'

Before do
  WebDriver.use_mob_proxy = true
  @browser = WebDriver.standard_browser
  WebDriver.mob_client.new_har 'Page load'
end

After do |scenario|
  if scenario.failed?
    screenshot_location = "logs/scenario_#{scenario.name}.png"
    @browser.screenshot.save screenshot_location
    attach screenshot_location, 'image/png'
  end
end

at_exit do
  WebDriver.close
  puts 'Shutting servers down'
  sleep 1
  Process.kill('KILL', ENV['test_server_pid'].to_i) if ENV['test_server_pid']
  Process.kill('KILL', ENV['web_server_pid'].to_i) if ENV['web_server_pid']
  sleep 1
end
