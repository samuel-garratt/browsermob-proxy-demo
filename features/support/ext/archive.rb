# frozen_string_literal: true

module HAR
  # Adding extensions to HAR Archive class
  class Archive
    # Return first entry starting with URL
    # @param [String] url Url to search HTTP archive requests for
    # @return [HAR::Archive::Entry] Return first entry starting with this URL
    def entry_starting_with(url)
      first_entry = entries_starting_with(url).first
      raise 'No har entries found' unless first_entry
    end

    # @param [String] url Url to search HTTP archive requests for
    # @return [Array] Return all entries starting with a URL
    def entries_starting_with(url)
      entries.find_all do |entry|
        entry.request.url.start_with?(url)
      end
    end

    # @param [String] url Url to search HTTP archive requests for
    # @return [Array] Return all requests starting with a URL
    def requests_starting_with(url)
      entries_starting_with(url).map(&:request)
    end
  end
end
