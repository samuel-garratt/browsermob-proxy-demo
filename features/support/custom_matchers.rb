# frozen_string_literal: true

RSpec::Matchers.define :be_successful do
  # Matched if response has a successful HTTP status code
  match do |response|
    raise "Response with status code of #{response.status} is not a successful satus code" unless (200..300).cover? response.status

    true
  end
end
