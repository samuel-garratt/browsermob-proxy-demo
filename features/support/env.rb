# frozen_string_literal: true

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '../../', 'lib'))

require 'rspec'
require 'page-object'
require 'browsermob-proxy'
require 'require_all'

require_all 'lib'

World(PageObject::PageFactory)

# Re write packages URL to passed in path
# @param [String] path Path at mock server to rewrite to
def use_mock(path)
  WebDriver.mob_client.rewrite 'http://localhost:4000/packages.json', "http://localhost:2000/#{path}"
end
