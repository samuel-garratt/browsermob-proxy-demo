# frozen_string_literal: true

Given('there are some packages in the backend') do
  puts 'We will assume this to be true'
end

When('I visit the packages page') do
  visit(PackagesPage)
end

Given('I have {int} packages') do |number_of_packages|
  use_mock "packages/#{number_of_packages}"
end

Then('it should show {int} package(s)') do |package_number|
  expect(@current_page.packages.count).to eq package_number
end

And 'the package address should be completely visible' do
  expect(@current_page.packages.first.text).to end_with 'displayed on the screen'
end

And 'the package orientation should be Portrait' do
  expect(@current_page.packages.first.text).to include 'Orient: PORTRAIT'
end

Then 'the correct GET request should have been made to the backend' do
  har = WebDriver.mob_client.har
  @backend_call = har.entry_starting_with 'http://localhost:4000/packages'
  expect(@backend_call.request).not_to eq nil
  expect(@backend_call.request.url).to eq 'http://localhost:4000/packages.json'
  expect(@backend_call.request.method).to eq 'GET'
end

Then 'the response should be successful' do
  expect(@backend_call.response).to be_successful
end

Then "a message should indicate there's no more packages to deliver" do
  expect(@current_page.text).to include 'No packages to deliver'
end
