# frozen_string_literal: true

Given 'I visit the post code finder page' do
  visit PostCodeFinderPage
end

Given('I have an address with {int} suggestions matching it') do |number_of_suggestions|
  use_mock "addresses/#{number_of_suggestions}"
end

When('I type part of an address') do
  on(PostCodeFinderPage).address = '20 Langdale Ave'
end

When 'I type the address' do
  step 'I type part of an address'
end

Then 'the correct address suggestion should be requested' do
  sleep 2.5 # Wait for last request to be made
  har = WebDriver.mob_client.har
  har.save_to 'address.har'
  query_entries = har.requests_starting_with 'https://address.nzpost.co.nz/api/suggest'
  query_entries.each { |entry| puts entry.url }
  query = query_entries.last
  query_params = query.query_string
  # expect(query_params.count).to eq 6 You could count the number of parameters.

  expect(query_params[1]).to eq('name' => 'q', 'value' => '20 Langdale Ave')
  expect(query_params.last).to eq('name' => 'max', 'value' => '5')
end

Then('{int} suggestions should be retrieved') do |expected_suggestion_count|
  sleep 30
  expect(on(PostCodeFinderPage).autocomplete.suggestions.count).to eq expected_suggestion_count
end
