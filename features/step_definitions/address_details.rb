# frozen_string_literal: true

Given 'I visit the address details page' do
  visit AddressDetailsFormPage
end

And 'I fill in the form' do
  on(AddressDetailsFormPage) do |page|
    @data = { dpid: '1616106', public_api_key: '56331ff0-c79a-012e-d172-005056013e1c' }
    Watir::Wait.until(timeout: 30) { page.submit_element.exists? }
    page.format = 'XML'
    page.populate_page_with @data
  end
end

When 'I submit the form' do
  WebDriver.mob_client.new_har 'TestHar' # Har should be fresh before submit
  on(AddressDetailsFormPage).submit
end

Then 'the correct parameters will be passed' do
  har = WebDriver.mob_client.har
  har.save_to 'address_details.har'
  pending "Exercise: Verify all parameters at #{@data} are sent correctly"
end
