Feature: Address Details

  This feature is for someone to try and implement themselves as a practice.
  The last step in the scenario is pending.

  Scenario: Send correct request to obtain address details
    Given I visit the address details page
    And I fill in the form
    When I submit the form
    Then the correct parameters will be passed
