Feature: Packages
  The most recent packages should be displayed so I know where to deliver them.

  Scenario: Package count should have a max of 10
    Given I have 11 packages
    When I visit the packages page
    Then it should show 10 packages

  Scenario: No packages should indicate there's no more to deliver
    Given I have 0 packages
    When I visit the packages page
    Then a message should indicate there's no more packages to deliver

  Scenario: API makes correct GET request
    When I visit the packages page
    Then the correct GET request should have been made to the backend
    And the response should be successful