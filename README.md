# browsermob-proxy-demo

Demonstration on how to use BrowserMobProxy to verify HTTP requests and redirect traffic to a virtual service to determine a page handles requests correctly.

## Presentation

Hosted by gitpitch at https://gitpitch.com/samuel-garratt/browsermob-proxy-demo?grs=gitlab#/

## Run tests

rake will execute tests. See .gitlab-ci.yml for details on docker image and how things are configured

[![Build Status](https://gitlab.com/samuel-garratt/browsermob-proxy-demo/badges/master/pipeline.svg)](https://gitlab.com/samuel-garratt/browsermob-proxy-demo/pipelines)

## Exercise

Finish the last step of the pending scenario, `features/address_details.feature` by checking Request parameters are correct

# Updating Dockerfile

* Google chrome in Dockerfile needs to be updated regularly. This should be done on CI later
* Rebuild image `docker build . -t samuelgarratt/rubymob:latest --no-cache`
* Push image `docker push samuelgarratt/rubymob:latest`