# frozen_string_literal: true

# Page object for representing the page under test in UAT
class PackagesPage
  include PageObject

  page_url 'http://localhost:4000'

  # @return [PageObject::Elements::ListItemElements] List of items within the packages unordered list
  def packages
    unordered_list_element(id: 'packages').list_item_elements
  end
end
