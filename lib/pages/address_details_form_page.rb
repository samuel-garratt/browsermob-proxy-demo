# frozen_string_literal: true

require_relative 'form_page'

# Class for using the Address Dev tools API
class AddressDetailsFormPage < FormPage
  include PageObject

  unique_url_id 'details'

  h2(:suggest_form, text: 'Address Details Service Form') # Text to appear on page
  h2(:service_header, text: 'Address Details Service') # Text to appear on page
end
