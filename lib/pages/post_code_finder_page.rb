# frozen_string_literal: true

# Section representing a list of autocomplete suggestions
class AutoCompleteList
  include PageObject

  # @return [SpanCollection] List of span elements for all the autocomplete address suggestions
  def suggestions
    span_elements(class: 'autocomplete-address')
  end
end

# Class representing the new Post code finder page
class PostCodeFinderPage
  include PageObject

  page_url 'https://www.nzpost.co.nz/tools/address-postcode-finder'

  text_field(:address, id: 'edit-address')
  page_section(:autocomplete, AutoCompleteList, class: 'autocomplete')
end
