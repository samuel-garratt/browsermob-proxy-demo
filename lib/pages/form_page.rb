# frozen_string_literal: true

# Common methods shared across classes that inherit FormPage
module CommonFormMethods
  # Used to form URL to goto. Goto method defined as part of this method
  # Position is after 'api' and before 'docs'
  # @param [String] url_id Unique part of URL to identify page to navigate to
  def unique_url_id(url_id)
    # Common goto method for all Docs Pages
    define_method(:goto) do
      @browser.goto "http://address.nzpost.co.nz/api/#{url_id}/docs"
      sleep 5
      open_actual_page
      # Watir::Wait.until(message: "'Try it here' expected service not seen. URL is #{@browser.url}", timeout: 30) do
      #   sleep 1 # Delay then refresh added as sometimes URL not gone to
      #   @browser.refresh
      #   # open_page_if_outside
      # end
    end
  end
end

# Inherit this class to have all functionality common to an addressing FormPage
class FormPage
  include PageObject
  extend CommonFormMethods

  # If link to actual page is present then click it and you should be on the right page
  def open_page_if_outside
    if open_actual_page?
      sleep 1
      @browser.refresh
      sleep 1
      open_actual_page
      true
    else
      puts 'INFO: needs a refresh'
      false
    end
  end

  # clicking here link top of page
  link(:clicking_here, id: 'prepopulate_link')

  # Address Details Form - Variables on the form page
  text_field(:public_api_key, id: 'public_api_key')
  text_field(:private_api_key, id: 'private_api_key')
  text_field(:dpid, id: 'dpid')
  link(:open_actual_page, text: 'Try it here')
  select_list(:format, id: 'format') # Format JSON or XML
  text_field(:callback, id: 'callback')
  text_field(:query_stub, id: 'q')
  text_field(:max, id: 'max')
  select_list(:type, id: 'type') # All/Postal or Physical

  # Submit Button
  button(:submit, xpath: '//input[@value ="submit"]')
end
