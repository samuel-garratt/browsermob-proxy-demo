# frozen_string_literal: true

require 'watir' # Wraps around selenium to control web browser
require 'rest-client'
require 'webdrivers'

# Helps with providing browsers for automation
module WebDriver
  @standard_browser = nil
  @user_data_folder = nil
  @use_browser_mob = false
  @mob_client = nil
  @corp_proxy = true

  BROWSER_MOB_EXTERNAL_PORT = 8080
  BROWSER_MOB_INTERNAL_PORT = 8081

  class << self
    # Folder to use for specific profile. If nil then empty profile will be used each time
    attr_accessor :user_data_folder
    # @!attribute Whether to use browser mob proxy for tests
    attr_accessor :use_mob_proxy
    # @!attribute Browser Mob client communicating with BrowserMobProxy session
    attr_writer :mob_client

    # A har (Http Archive) can be recorded and obtained with this client
    # @return [BrowserMob::Proxy::Client] Client communicating with BrowserMobProxy session
    def mob_client
      @mob_client || raise('Browser Mob proxy not set. Set with "Nzp::WebDriver.use_browser_mob = true"')
    end

    # Returns/Creates a browser to be used across the scope of the entire regression suite.
    # This may be used across several Cucumber features (for example)
    # @param [Array] options Options that can be passed to override Chrome defaults
    # @return [Watir::Browser] Return the current Watir browser or create a new one
    def standard_browser(options = [])
      retry_count = 0
      begin
        @standard_browser ||= chrome(options)
      rescue RestClient::RequestFailed => e
        retry_count += 1
        retry unless retry_count > 3
        raise e
      end
      @standard_browser.window.maximize
      @standard_browser
    end

    # Close the generic browser and set the instance variable to nil so that it can be refreshly created
    def close
      return 'Browser closed without being set' unless @standard_browser

      if use_mob_proxy
        mob_client.close
        sleep 3 # Wait to ensure browsermob closed before next test
      end
      @standard_browser.close
      @standard_browser = nil # Reset browser so will be reset again if needed
    end

    # Depending on OS and whether being run from Jenkins, the user data directory will be different
    # @return [String] User data directory used by chrome
    def user_data_dir
      if ENV['jenkins']
        File.join('D:/jenkins/chrome', user_data_folder)
      else
        Platform.windows? ? "C:/Users/#{ENV['USERNAME']}/AppData/Test/Chrome" : '~/.config/google-chrome/Test'
      end
    end

    def firefox
      server = BrowserMob::Proxy::Server.new(*browsermob_proxy_settings)
      server.start

      proxy = server.create_proxy
      profile = Selenium::WebDriver::Firefox::Profile.new #=> #<Selenium::WebDriver::Firefox::Profile:0x000001022bf748 ...>
      profile.proxy = proxy.selenium_proxy

      Watir::Browser.new :firefox, profile: profile
    end

    # Returns Watir browser capabilities, setting browser Mob proxy settings if necessary
    # @return [Hash] Capability of Watir browser
    def watir_capabilities
      return {} unless use_mob_proxy

      server = BrowserMob::Proxy::Server.new(*browsermob_proxy_settings)
      puts "Using BrowserMob settings: #{browsermob_proxy_settings}"
      server.start
      self.mob_client = server.create_proxy
      proxy = Selenium::WebDriver::Proxy.new(http: mob_client.selenium_proxy.http, ssl: mob_client.selenium_proxy(:ssl).ssl)
      { proxy: proxy }
    end

    # Returns a chrome Watir browser.
    # Watir (http://watir.com/) uses Selenium (www.seleniumhq.org). It has methods that makes working with faster, more efficient and easier to read
    # See https://github.com/SeleniumHQ/selenium/wiki/Ruby-Bindings for details on Selenium browser options
    # @param [Hash] extra_options Options that can be passed to override Chrome defaults
    # @return [Watir::Browser] a Watir 'browser' object. Usually this is passed to '@browser' which is a special variable name that PageFactory uses
    def chrome(extra_options = [])
      # Create an options variable to allow various configurations to be made to the Chrome browser.
      options = Selenium::WebDriver::Chrome::Options.new(args: %w[--disable-popup-blocking --ignore-certificate-errors --disable-notifications])
      options.add_argument("--user-data-dir=#{user_data_dir}") if user_data_folder
      options.add_argument('--no-sandbox')
      options.add_argument('--disable-dev-shm-usage')
      extra_options.each { |option| options.add_argument option }
      # Selenium::WebDriver::Chrome.driver_path = `which chromedriver`.strip
      Watir::Browser.new :chrome, options: options, **watir_capabilities
    end

    # Finding an executable in the $PATH.
    # @return [String] Path to where executable is
    def which(cmd)
      `which #{cmd}`.strip
    end

    private

    # @return [String] Name of '.bat' file to run browsermob-proxy. The 'bat' file without-proxy will not use the CORP proxy
    def browsermob_proxy_settings
      [which('browsermob-proxy'), port: BROWSER_MOB_INTERNAL_PORT, log: true]
    end
  end
end
