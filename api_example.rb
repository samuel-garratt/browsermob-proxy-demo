# frozen_string_literal: true

require 'rest-client'

puts 'API example without proxy'
puts RestClient.get('http://portquiz.net/')

puts 'Make proxy server at port 8095'
require 'browsermob-proxy'
server = BrowserMob::Proxy::Server.new(`which browsermob-proxy`.strip,
                                       port: 8095)
server.start
puts 'Create proxy client at port 8096'
mob_client = server.create_proxy(8096)

puts 'Set up virtual response at port 2000'
virtual_process = Process.spawn('ruby', 'mock_response.rb',
                                err: %w[logs/test_server.log w])
sleep 2 # Wait for process to start

puts 'Client redirect to virtual response'
mob_client.rewrite('http://portquiz.net/',
                   'http://localhost:2000')
RestClient.proxy = 'http://localhost:8096'
puts 'API example with proxy redirecting to virtual response'
puts RestClient.get('http://portquiz.net/')

Process.kill('KILL', virtual_process)
