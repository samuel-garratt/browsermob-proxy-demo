# BrowserMob Proxy demo

---

## Where BrowserMob Proxy sits

![Where BrowserMobSits for UI Test](assets/img/WhereBrowserMobSits.png)

---

## What BrowserMobProxy can do

* Monitoring HTTP/HTTPS/FTP traffic so that assertions can be done
* Rewriting to a different URL (in this case, a virtual service)
* Adding Basic Auth
* Much more

---

## What this enables

* Tests to ensure that actions on a UI result in a backend request rather than inferring it through another result
  later on. This enables tests to be done at an earlier level
* Waiting for a particular HTTP request (rather than a manual sleep)

---

## What this enables

* Visualisation of HTTP traffic through a HAR (HTTP Archive)
* Redirecting traffic from a backend to a virtual server enabling you to test how a UI handles different backend responses

---?code=features/postcode_finder.feature&lang=gherkin&title=Test Https Request

@[1-6](Scenario checking request is correct)
@[7](Visit the site)
@[8](Make an action that will trigger an HTTP request)
@[9](Verify HTTP request sent has correct parameters)
@[1-10](Now let's see it run)

---

---?code=features/packages.feature&lang=gherkin&title=Redirect to virtual service

@[4](Scenario for which I need a certain response from backend)
@[5](Set up browsermob client to point to response with that data needed)
@[6](Visit the page, invoking the backend call)
@[7](Expected result that the page correctly limits what's displayed to 10)

---

## Api

* This proxy could also be used for helping test APIs
* One of the benefits is that you have one proxy server and each test has a seperate client
* One test could redirect to a certain URL and another not change anything

---

---?code=api_example.rb&lang=ruby&title=API example walkthrough

@[1-4](Example of calling API normally)
@[6-10](Setup BrowserMobProxy server)
@[11-12](Create a client using this server)
@[14-16](Start a virtual response)
@[19-21](Setup traffic redirection on BrowerMob client)
@[22](Use BrowserMobProxy client as proxy)
@[23-24](See the result. The response is returned from the virtual service)

---

## Test without changing code
* Say there is a middleware application as part of a large integrated test environment
* One critical middleware system could be set to use a BrowserMobProxy client as it's proxy
* Each test team could have their own instance of the application and set it to their own
  client. They could then redirect to virtual services as their tests need them

---

## Limitations

* Cannot rewrite the host or port of an HTTPS connection.
  Has to be HTTP. Though HTTPS traffic can be monitored.
  See https://github.com/lightbody/browsermob-proxy/blob/master/browsermob-core/src/main/java/net/lightbody/bmp/filters/RewriteUrlFilter.java
  for details

---

## Questions

* Regarding BrowserMob Proxy
* Regarding Virtual Server used
* GitPitch
* Anything else
