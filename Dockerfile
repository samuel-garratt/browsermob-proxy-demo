FROM samuelgarratt/rubymob:latest
MAINTAINER Samuel Garratt
LABEL url="https://gitlab.com/samuel-garratt/browsermob-proxy-demo/blob/master/Dockerfile"

RUN mkdir /mysuite
WORKDIR /mysuite
COPY Gemfile /mysuite/Gemfile
RUN gem install bundler rake
RUN bundle install
