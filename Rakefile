# frozen_string_literal: true

require 'rubygems'
require 'cucumber'
require 'cucumber/rake/task'

task(:logs) { mkdir_p 'logs' }

Cucumber::Rake::Task.new(features: %w[start_test_server web_server logs])

Cucumber::Rake::Task.new(postcode: :logs) do |t|
  t.cucumber_opts = 'features/postcode*.feature'
end

Cucumber::Rake::Task.new(address_details: :logs) do |t|
  t.cucumber_opts = 'features/address_details.feature'
end

Cucumber::Rake::Task.new(packages: %w[start_test_server web_server logs]) do |t|
  t.cucumber_opts = 'features/packages.feature'
end

task default: :features

desc 'Start virtual web service'
task start_test_server: 'logs' do
  ENV['test_server_pid'] = Process.spawn('ruby', 'mock_response.rb', err: %w[logs/test_server.log w]).to_s
  sleep 4 # Wait a little for virtual server to start up
  puts 'Running test server at pid ' + ENV['test_server_pid']
end

task web_server: 'logs' do
  ENV['web_server_pid'] = Process.spawn('bundle', 'exec', 'jekyll', 'serve', '--source', 'packages',
                                        err: %w[logs/web_server.log w]).to_s
  sleep 2 # Wait a little for virtual server to start up
  puts 'Running web server at pid ' + ENV['web_server_pid']
end
