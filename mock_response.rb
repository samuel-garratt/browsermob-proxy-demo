# frozen_string_literal: true

require 'sinatra'
require 'json'

set :port, 2000

get '/' do
  'custom response from virtual service'
end

get '/packages/:count' do
  result_list = []
  params['count'].to_i.times do |iteration|
    result = { carrier: 'PACE', orientation: 'LANDSCAPE', tracking_number: "TST1539816970#{iteration}NZ", loc: '9251 Macejkovic Alley' }
    result_list << result
  end
  json_hash = { results: result_list }
  JSON.generate json_hash
end
